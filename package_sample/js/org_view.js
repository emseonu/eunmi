inapp.add( "org_view", {
    init: function () {
        this.create();
        this.click_sync();
        this.expand_sync();
        this.button();
    },
    actionMap: { //tree_view에서 받아온 값으로 event처리
        "tree_view": {
            "click_tree": function ( prop ) {
                this.click_sync( prop.data );
            },
            "expand_tree": function ( prop ) {
                this.expand_sync( prop.data.key, prop.data.expand );
            }
        },
        "popup": { //popup에서 받아온 값으로 event처리
            "save_popup": function ( prop ) {
                this.save( prop.data );

            }
        }
    },
    create: function () {
        var option, that;
        that = this; //inapp.raise에서 org_view 대신 쓰기위함

        option = {
            layout: {
                level: true, // 레벨 정렬 사용여부
                supporter: true // 보조자 사용여부
            },
            defaultTemplate: {
                link: { // 연결선 스타일
                    style: {
                        borderWidth: 2,
                        borderColor: "#CCC",
                        borderStyle: "solid"
                    }
                }
            },
            event: {
                onClick: function ( evt ) {
                    var select_key, node, json_data;

                    // 노드가 선택되지 않았을 땐, 처리하지 않는다.
                    // 이벤트가 노드관련해서만 있기 때문에.
                    if ( !evt.key ) {
                        return;
                    }

                    // 멤버 여부에 따라 노드를 구분해서 node 변수에 담는다.
                    if ( evt.isMember == true ) {
                        node = evt.member;
                    } else {
                        node = evt.node;
                    }

                    json_data = {};

                    // 노드 필드 데이터를 datas에 저장한다.
                    node.fields().foreach( function () {
                        json_data[ this.name() ] = this.value();
                    } );

                    // 보조자 여부를 추가한다.
                    json_data[ "supporter" ] = node.supporter();

                    // 노드가 선택되었음을 다른 화면(tree, popup 등)에 전달한다.
                    //data보낼때 tree, popup에서 사용할것 나누지 말고
                    //보낸뒤 받는곳에서 처리
                    inapp.raise( that.name, {
                        action: "click_node",
                        data: {
                            key: evt.key,
                            binding: evt.binding,
                            data: json_data
                        }
                    } );

                },
                onExpandButtonClick: function ( evt ) {
                    inapp.raise( that.name, {
                        action: "expand_node",
                        data: {
                            key: evt.key,
                            expand: evt.isExpand
                        }
                    } );
                },
                onDiagramLoadEnd: function ( evt ) {
                    var jsonData, data;
                    // 조직도 데이터를 트리 데이터 형식으로 가공
                    data = [];

                    /// 아래와 같이 구성하면 에러나요!
                    /*
                    jsonData = {
                        data
                    };
                    */

                    jsonData = {};

                    // foreach를 통해 노드의 필드 데이터를 추출(라벨과 멤버 노드는 제외)
                    evt.org.nodes().foreach( function () {
                        if ( !this.isLabel() && !this.isMember() ) {
                            data.push( {
                                "dept_cd": this.fields( "pkey" ).value(),
                                "dept_nm": this.fields( "dept_nm" ).value(),
                                "emp_nm": this.fields( "emp_nm" ).value(),
                                "Level": this.level() - 1,
                            } );
                        }
                    } );
                    inapp.raise( that.name, {
                        action: "load_node",
                        data: jsonData
                    } );
                }
            }
        }

        createINOrg( "viewOrg", option );

        this.load();
    },
    load: function () {
        viewOrg.loadJson( {
            url: "./data/sample.json"
        } );
    },
    click_sync: function ( select_key ) {
        viewOrg.nodes( select_key ).select( true );
    },
    expand_sync: function ( select_key, expand ) {
        viewOrg.nodes( select_key ).expand( expand );
    },
    save: function ( setData ) {
        var supporToInt, node, pkey;
        pkey = setData.pkey;
        node = viewOrg.nodes( pkey );
        supporToInt = parseInt( setData.supporter ); /// 왼쪽과 같이 사용해도 무방하지만, 좀더 정확한 표현으로 10진수 변환을 넣어서 오른쪽과 같이 표기하는게 좋아요 > parseInt( setData.supporter , 10);

        if ( setData.supporter !== "" ) {
            node.supporter( supporToInt );
        } else {
            node.supporter( null );
        }
        node.fields( "emp_nm" ).value( setData.emp_nm );
        node.fields( "position" ).value( setData.position );
        node.fields( "dept_nm" ).value( setData.dept_nm );

    },

    button: function () {
        that = this;
        $( "#modi_temp" ).on( "change", function () {
            that.modi_temp();
        } );

        $( "#zoomIn" ).on( "click", function () {
            that.zoomIn();
        } );

        $( "#zoomOut" ).on( "click", function () {
            that.zoomOut();
        } );

        $( "#scaleFit" ).on( "click", function () {
            that.scaleFit();
        } );

        $( "#scaleFullfit" ).on( "click", function () {
            that.scaleFullfit();
        } );

        $( "#modi_level_up" ).on( "click", function () {
            that.modi_level_up();
        } );

        $( "#modi_level_down" ).on( "click", function () {
            that.modi_level_down();
        } );

        $( "#saveAsImage" ).on( "click", function () {
            that.saveAsImage();
        } );

        $( "#saveAsJson" ).on( "click", function () {
            that.saveAsJson();
        } );

        $( "#level_switch" ).on( "change", function () {
            var val;

            val = $( this ).val(); /// level_switch 에서 변경된 값을 변수 val 에 저장
            val = ( val == "true" ) ? true : false;

            that.level_switch( val );

            /// that.level_switch();
        } );

        $( "#support_switch" ).on( "change", function () {
            var val;

            val = $( this ).val(); /// support_switch 에서 변경된 값을 변수 val 에 저장
            val = ( val == "true" ) ? true : false;

            that.support_switch( val );

            /// that.support_switch();
        } );
    },

    modi_temp: function () {
        viewOrg.nodes().foreach( function () {
            if ( !this.isLabel() ) {
                this.template( modi_temp.value );
            }
        } );
    },
    zoomIn: function () {
        viewOrg.zoomIn( true );
    },

    zoomOut: function () {
        viewOrg.zoomOut( true );
    },
    scaleFit: function () {
        viewOrg.scale( "fit" );
    },

    scaleFullfit: function () {
        viewOrg.scale( "fullfit" );
    },

    modi_level_up: function () {
        var node;
        node = viewOrg.nodes.selected(); /// 반복해서 쓰이는 것은 하나의 변수에 담아서 처리하는게 가독성 측면에서 좋아요!

        node.level( node.level() - 1 );

        // viewOrg.nodes.selected().level( viewOrg.nodes.selected().level() - 1 );
    },
    modi_level_down: function () {
        var node;
        node = viewOrg.nodes.selected();

        node.level( node.level() + 1 );

        // viewOrg.nodes.selected().level( viewOrg.nodes.selected().level() + 1 );
    },
    saveAsImage: function () {
        viewOrg.saveAsImage( {
            "filename": "saveImage",
            "type": "jpg"
        } );
    },
    saveAsJson: function () {
        viewOrg.saveAsJson();
    },
    level_switch: function ( val ) {
        /// level_switch 와 같이 접근하는 것은 다른 브라우저에 접근 에러가 날 수 있으므로 JQuery 를 사용해서 아래와 같이 구성하세요.
        /// level_switch를 사용한다면 이렇게 사용 > $('#level_switch');

        /// val 만 넘겨서 처리하는게 좀더 깔끔하게 코드를 구성할 수 있어요!
        viewOrg.layout.level( val );

        // if ( level_switch.value == "true" ) {
        //     viewOrg.layout.level( true );
        // } else {
        //     viewOrg.layout.level( false );
        // }

    },
    support_switch: function ( val ) {
        /// 위의 level_switch와 동일
        viewOrg.layout.supporter( val );

        // if ( support_switch.value == "true" ) {
        //     viewOrg.layout.supporter( true );
        // } else {
        //     viewOrg.layout.supporter( false );
        // }

    },
} )