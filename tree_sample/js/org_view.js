inapp.add( "org_view", {
    init: function () {
        this.create();
        this.click_sync();
        this.expand_sync();
    },
    actionMap: { //tree_view에서 받아온 값으로 event처리
        "tree_view": {
            "click_tree": function ( prop ) {
                this.click_sync( prop.data );
            },
            "expand_tree": function ( prop ) {
                this.expand_sync( prop.data.key, prop.data.expand );
            }

        }

    },
    create: function () {
        var option, that;
        that = this; //inapp.raise에서 org_view 대신 쓰기위함

        option = {
            layout: {
                level: true, // 레벨 정렬 사용여부
                supporter: true // 보조자 사용여부
            },
            defaultTemplate: {
                link: { // 연결선 스타일
                    style: {
                        borderWidth: 2,
                        borderColor: "#CCC",
                        borderStyle: "solid"
                    }
                }
            },
            event: {
                onClick: function ( evt ) {
                    var select_key;
                    select_key = evt.org.nodes.selected().pkey();

                    inapp.raise( that.name, {
                        action: "click_node",
                        data: select_key
                    } );

                },
                onExpandButtonClick: function ( evt ) {
                    inapp.raise( that.name, {
                        action: "expand_node",
                        data: {
                            key: evt.key,
                            expand: evt.isExpand
                        }
                    } );
                },
                onDiagramLoadEnd: function ( evt ) {
                    var jsonData, data;
                    // 조직도 데이터를 트리 데이터 형식으로 가공
                    data = [];
                    jsonData = {
                        data
                    };
                    // foreach를 통해 노드의 필드 데이터를 추출(라벨과 멤버 노드는 제외)
                    evt.org.nodes().foreach( function () {
                        if ( !this.isLabel() && !this.isMember() ) {
                            data.push( {
                                "dept_cd": this.fields( "pkey" ).value(),
                                "dept_nm": this.fields( "dept_nm" ).value(),
                                "emp_nm": this.fields( "emp_nm" ).value(),
                                "Level": this.level() - 1,
                            } );
                        }
                    } );
                    inapp.raise( that.name, {
                        action: "load_node",
                        data: jsonData
                    } );
                }
            }
        }

        createINOrg( "viewOrg", option );

        this.load();
    },
    load: function () {
        viewOrg.loadJson( {
            url: "./data/sample.json"
        } );
    },
    click_sync: function ( select_key ) {
        viewOrg.nodes( select_key ).select( true );
    },
    expand_sync: function ( select_key, expand ) {
        viewOrg.nodes( select_key ).expand( expand );
    }
} )