inapp.add( "chart", {
    cfg: {
        dataLoaded: false
    },
    actionMap: {
        "data_manage": {
            "data_loaded": function ( prop ) {
                var set, data;
                set = this.set_data;
                data = prop.data;
                this.cfg.dataLoaded = true; //데이터가 로드되었는가
                set( "#doc1", data.year );
                set( "#doc2", data.salary );
                set( "#doc3", data.incen );
                set( "#doc4", data.cost );
                set( "#doc5", data.person );
                set( "#doc6", data.rate );
                set( "#doc7", data.sex );
                set( "#doc8", data.age );
                set( "#doc9", data.sc );
                set( "#doc10", data.type );
                set( "#doc11", data.class );
                set( "#doc12", data.kind );
                set( "#doc13", data.work );
                set( "#doc14", data.vaca );
                set( "#doc15", data.get );
                set( "#doc16", data.sign );
            }
        }
    },
    init: function () {
        var set, chart1, chart2, chart3, chart4, chart5, chart6, chart7, chart8, chart9, chart10, chart11, chart12, chart13, chart14, chart15, chart16, horiBar_arr, donut_arr, pie_arr, picto_arr, bar_arr, opt_horiBar, opt_donut, opt_pie, opt_picto, opt_bar, charts_arr, i, len;
        horiBar_arr = [], donut_arr = [], pie_arr = [], picto_arr = [], bar_arr = [], charts_arr = [];

        //chart init
        set = echarts.init;
        chart1 = set( $( '#doc1' )[ 0 ] ); //근속년수
        chart2 = set( $( '#doc2' )[ 0 ] ); //연봉인상률
        chart3 = set( $( '#doc3' )[ 0 ] ); //인센티브
        chart4 = set( $( '#doc4' )[ 0 ] ); //인건비예산
        chart5 = set( $( '#doc5' )[ 0 ] ); //인원정보
        chart6 = set( $( '#doc6' )[ 0 ] ); //평가분포별 인원
        chart7 = set( $( '#doc7' )[ 0 ] ); //성별비율별 인원
        chart8 = set( $( '#doc8' )[ 0 ] ); //평균연령
        chart9 = set( $( '#doc9' )[ 0 ] ); //성별비율별 인건비
        chart10 = set( $( '#doc10' )[ 0 ] ); //직원유형별 비율 및 인원
        chart11 = set( $( '#doc11' )[ 0 ] ); //직군별 비율 및 인원
        chart12 = set( $( '#doc12' )[ 0 ] ); //직종별 비율 및 인원
        chart13 = set( $( '#doc13' )[ 0 ] ); //월별 평균 근로기간
        chart14 = set( $( '#doc14' )[ 0 ] ); //인당 월별 평균휴가 사용일수
        chart15 = set( $( '#doc15' )[ 0 ] ); //일별 평균 출퇴근 시간
        chart16 = set( $( '#doc16' )[ 0 ] ); //월별 입퇴자사 수

        charts_arr.push( chart1, chart2, chart3, chart4, chart5, chart6, chart7, chart8, chart9, chart10, chart11, chart12, chart13, chart14, chart15, chart16 );


        //resize
        len = charts_arr.length;
        $(window).on("resize",function(){
           for(i=0; i<len; i++){
            charts_arr[i].resize();
           }
        })


        horiBar_arr.push( chart1, chart2, chart3 ); //horizontal bar chart arr
        donut_arr.push( chart4 ); //donut chart arr
        pie_arr.push( chart6, chart7, chart9, chart10, chart11, chart12 ); //pie chart arr
        picto_arr.push( chart5, chart8 ); //pictorial chart arr
        bar_arr.push( chart13, chart14, chart15, chart16 ); //bar chart arr

        /*------------------------*chart basic options --------------------------------*/
        //horizontal bar chart option
        opt_horiBar = {
            tooltip: {},
            xAxis: {
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: false
                },
                type: 'value',
                splitNumber: 5,
                min: 0,
                max: 100,
                splitLine: {
                    show: false,
                    interval: 'auto'

                },
                scale: true
            },
            yAxis: {
                axisLine: {
                    show: false
                },
                data: []
            },
            grid: {
                show: true,
                top: 'top',
                backgroundColor: '#efefef',
                height: 33
            },
            barWidth: 15,
            series: [ {
                type: 'bar',
                data: [],
                itemStyle: {
                    color: '#2f4554'
                }
            } ]
        }

        //donut chart option
        opt_donut = {
            grid: {
                top: 'top'
            },
            height: '80%',
            title: {
                subtext: 'text',
                left: '49%',
                top: '29%',
                textAlign: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: '{b}: {c} ({d}%)',
                fontWeight: 'bold'
            },
            legend: {
                icon: 'circle',
                top: 'bottom',
                data: []
            },
            label: {
                formatter: '{d}% \n({c}) ',
                fontWeight: 'bold',
                color: '#666666'
            },
            series: [ {
                type: 'pie',
                radius: [ '37%', '70%' ],
                avoidLabelOverlap: true,
                label: {
                    position: 'outer',
                    alignTo: 'edge',
                    margin: '7%'
                },
                data: []
            } ]
        }

        //pie chart option
        opt_pie = {
            grid: {
                top: 'top'
            },
            height: '80%',
            tooltip: {
                trigger: 'item',
                formatter: '{b}: {c} ({d}%)',
                fontWeight: 'bold'
            },
            legend: {
                icon: 'circle',
                formatter: '',
                bottom: 0,
                data: []
            },
            label: {
                formatter: '{d}% \n({c}) ',
                fontWeight: 'bold',
                color: '#666666'
            },
            series: [ {
                type: 'pie',
                radius: [ '0%', '68%' ],
                avoidLabelOverlap: true,
                label: {
                    position: 'outer',
                    alignTo: 'edge',
                    margin: '7%'
                },
                data: []
            } ]
        }

        //pictorial chart option
        opt_picto = {
            legend: {
                data: []
            },
            tooltip: {},
            grid: {
                bottom: '90'
            },
            title: {
                text: ':',
                textStyle: {
                    fontSize: 14
                },
                left: '49%',
                top: '34%',
                textAlign: 'center'
            },
            yAxis: {
                splitLine: {
                    show: false
                },
                axisLabel: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                }
            },
            xAxis: {
                data: [],
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    margin: 45
                }
            },
            series: [ {
                type: 'pictorialBar',
                label: {
                    show: true,
                    position: 'bottom',
                    offset: [ 0, 0 ],
                    formatter: '{c}',
                    fontWeight: 'bold',
                    fontSize: 13,
                    color: '#cc0000',
                },
                data: []
            } ]
        }

        //bar chart option
        opt_bar = {
            tooltip: {},
            barWidth: '45%',
            legend: {
                icon: 'circle',
                data: [],
                bottom: 'bottom'
            },
            label: {
                show: true,
                textBorderWidth: 2,
                position: 'inside'
            },
            grid: {
                top: '30',
                width: '85%'
            },
            xAxis: {
                type: 'category',
                data: [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ]
            },
            yAxis: {
                nameLocation: 'middle',
                nameGap: 40,
                type: 'value',
                splitNumber: 3,
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
            },
            series: [ {
                type: 'bar'
            } ]
        }
        this.option( horiBar_arr, opt_horiBar );
        this.option( donut_arr, opt_donut );
        this.option( pie_arr, opt_pie );
        this.option( picto_arr, opt_picto );
        this.option( bar_arr, opt_bar );

    },
    option: function ( arr, option ) { //분류끼리 공통 옵션 적용
        var i, len;
        len = arr.length;

        for ( i = 0; i < len; i++ ) {
            unit = arr[ i ];
            unit.setOption( option );
        }
    },
    set_data: function ( container, option ) {
        echarts.getInstanceByDom( $( container )[ 0 ] ).setOption( option );
    }
} )