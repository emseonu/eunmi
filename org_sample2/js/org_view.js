var org_view = {
    init: function () {
        this.create();
        this.button();
    },

    create: function () {
        var option;

        option = {
            layout: {
                level: true, // 레벨 정렬 사용여부
                supporter: true // 보조자 사용여부
            },
            defaultTemplate: {
                link: { // 연결선 스타일
                    style: {
                        borderWidth: 2,
                        borderColor: "#CCC",
                        borderStyle: "solid"
                    }
                }
            },
            event: {
                onClick: function ( evt ) {
                    var node, getData;
                    getData = {};

                    if ( !evt.key ) { //키값있을때 노드만
                        return;
                    }
                    if ( evt.binding !== "emp_nm" ) { //노드의 이름값 선택시에만
                        return;
                    }
                    if ( evt.isMember == true ) {
                        node = evt.member;
                    } else {
                        node = evt.node;
                    }

                    node.fields().foreach( function () {
                        getData[ this.name() ] = this.value();
                    } );

                    // 2단계 : 노드의 보조자 정보도 전달
                    getData[ "supporter" ] = node.supporter();

                    // 1단계 : 팝업과 관련된 소스를 popup.show 함수로 옮겨서 실행
                    popup.show( getData );

                },
                onExpandButtonClick: function ( evt ) {
                    var sampleData;

                    /**
                     * 자식노드가 없고, 확장 버튼을 펼칠때 (+ > - 로 변경)
                     * 자식노드(sampleData)를 append 조회하라
                     */
                    if ( evt.expandable == true ) {

                        sampleData = {
                            "orgData": [ {
                                "template": "PhotoBox",
                                "layout": {
                                    "level": 4
                                },
                                "fields": {
                                    "pkey": "5555",
                                    "rkey": "3000",
                                    "dept_nm": {
                                        "value": "테스트",
                                        "style": {
                                            "backgroundColor": "#D46231"
                                        }
                                    },
                                    "position": "사원",
                                    "emp_nm": "테스트"
                                }
                            } ]
                        }

                        console.log( "loadJson" );

                        evt.org.loadJson( {
                            data: sampleData,
                            append: true
                        } );


                    }

                }
            }
        }

        createINOrg( "viewOrg", option );

        this.load();
    },

    save: function ( setData ) {
        var supporToInt, node, pkey;
        pkey = setData.pkey;
        node = viewOrg.nodes( pkey );
        supporToInt = parseInt( setData.supporter );

        if ( setData.supporter !== "" ) {
            node.supporter( supporToInt );
        } else {
            node.supporter( null );
        }
        node.fields( "emp_nm" ).value( setData.emp_nm );
        node.fields( "position" ).value( setData.position );
        node.fields( "dept_nm" ).value( setData.dept_nm );

    },

    button: function () {
        $( "#modi_temp" ).on( "change", function () {
            org_view.modi_temp();
        } );

        $( "#zoomIn" ).on( "click", function () {
            org_view.zoomIn();
        } );

        $( "#zoomOut" ).on( "click", function () {
            org_view.zoomOut();
        } );

        $( "#scaleFit" ).on( "click", function () {
            org_view.scaleFit();
        } );

        $( "#scaleFullfit" ).on( "click", function () {
            org_view.scaleFullfit();
        } );

        $( "#modi_level_up" ).on( "click", function () {
            org_view.modi_level_up();
        } );

        $( "#modi_level_down" ).on( "click", function () {
            org_view.modi_level_down();
        } );

        $( "#saveAsImage" ).on( "click", function () {
            org_view.saveAsImage();
        } );

        $( "#saveAsJson" ).on( "click", function () {
            org_view.saveAsJson();
        } );

        $( "#level_switch" ).on( "change", function () {
            org_view.level_switch();
        } );

        $( "#support_switch" ).on( "change", function () {
            org_view.support_switch();
        } );
    },

    modi_temp: function () {
        viewOrg.nodes().foreach( function () {
            if ( !this.isLabel() ) {
                this.template( modi_temp.value );
            }
        } );
    },
    zoomIn: function () {
        viewOrg.zoomIn( true );
    },

    zoomOut: function () {
        viewOrg.zoomOut( true );
    },
    scaleFit: function () {
        viewOrg.scale( "fit" );
    },

    scaleFullfit: function () {
        viewOrg.scale( "fullfit" );
    },

    modi_level_up: function () {
        viewOrg.nodes.selected().level( viewOrg.nodes.selected().level() + 1 );
    },
    modi_level_down: function () {
        viewOrg.nodes.selected().level( viewOrg.nodes.selected().level() - 1 );
    },
    saveAsImage: function () {
        viewOrg.saveAsImage( {
            "filename": "saveImage",
            "type": "jpg"
        } );
    },
    saveAsJson: function () {
        viewOrg.saveAsJson();
    },
    level_switch: function () {
        console.log( viewOrg.layout.level() );
        if ( level_switch.value == "true" ) {
            viewOrg.layout.level( true );
        } else {
            viewOrg.layout.level( false );
        }

    },
    support_switch: function () {
        console.log( viewOrg.layout.supporter() );
        if ( support_switch.value == "true" ) {
            viewOrg.layout.supporter( true );
        } else {
            viewOrg.layout.supporter( false );
        }

    },

    load: function () {
        viewOrg.loadJson( {
            url: "./data/sample.json"
        } );
    }
}

$( document ).ready( function () {
    console.log( "시작" );
    org_view.init();
} );