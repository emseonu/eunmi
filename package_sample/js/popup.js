inapp.add( "popup", {

    init: function () {
        var that;
        that = this;
        // JQuery 버튼 클릭 메서드는 한번만 호출하면 된다.
        $( "#btnSave" ).on( "click", function () {
            // save 함수 호출
            that.save();
        } );
    },
    actionMap: { //orgview 받아온 값으로 event처리
        "org_view": {
            "click_node": function ( prop ) {
                /// 반복해서 쓰일 것들은 하나의 변수에 넣어서 처리하는게 좋아요!!
                var data;
                data = prop.data;

                //binding이 emp_nm 일때팝업 show
                if ( data.binding == "emp_nm" ) {
                    this.show( data.data );
                }

                //binding이 emp_nm 일때팝업 show
                // if ( prop.data.binding !== "emp_nm" ) {
                //     return;
                // }
                // this.show( prop.data.data );
            }
        }
    },
    /**
     * 팝업을 보여준다.
     */
    show: function ( getData ) {
        $( '#modalExample' ).modal( 'show' );
        $( '#pkey' ).val( getData.pkey );
        $( '#photo' ).attr( "src", getData.photo );
        $( '#emp_nm' ).val( getData.emp_nm );
        $( '#position' ).val( getData.position );
        $( '#dept_nm' ).val( getData.dept_nm );
        $( '#supporter' ).val( getData.supporter );
    },
    /**
     * 팝업 내에 저장 버튼을 누를때
     */
    save: function () {
        // 3단계 : 저장시 팝업 필드 정보를 선택된 노드에 반영
        var setdata;

        setdata = {
            "pkey": $( '#pkey' ).val(),
            "emp_nm": $( '#emp_nm' ).val(),
            "position": $( '#position' ).val(),
            "dept_nm": $( '#dept_nm' ).val(),
            "supporter": $( '#supporter' ).val()
        };

        inapp.raise( "popup", {
            action: "save_popup",
            data: setdata
        } );


    }


} )