inapp.add( "tree_view", {
    cfg: {},
    init: function () {
        this.create();
        this.click_sync();
        this.expand_sync();
    },
    actionMap: { //org_view에서 받아온 값으로 event처리
        "org_view": {
            "click_node": function ( prop ) {
                this.click_sync( prop.data );
            },
            "load_node": function ( prop ) {
                this.load( prop.data );
            },
            "expand_node": function ( prop ) {
                this.expand_sync( prop.data.key, prop.data.expand );
            }

        }

    },
    createSheet: function () {
        /**
         * IBSheet를 생성
         * @param 	{object}				IBSheet가 생성될 대상 컨테이너
         * @param	{object}				IBSheet의 ID
         * @param	{String}	"100%"		너비
         * @param	{String}	"100%"		높이
         */

        //treeArea에 treeSheet를 100%로 생성
        createIBSheet2( document.getElementById( "treeArea" ), "treeSheet", "100%", "100%" );
    },
    create: function () {
        // 개발자 가이드 - 5.1 항목을 보고 작성
        var option = {};

        this.createSheet();
        //json형태로 구성하여 전달
        option = {
            Cfg: { //시트 기본 설정
                SearchMode: smLazyLoad, //조회방식설정 : 스크롤 조회
                Page: 30, //스크롤 조회에서 한번에 표시할 행의 개수 -> 스크롤에 따라 30개씩 조회
                AutoFitColWidth: "search|resize|init" //특정 시점(""->default)에서 컬럼의 너비를 자동으로 조정 -> 조회 및 로드 | 시트resize | 초기화 및 RemoveAll시점

            },
            HeaderMode: { //header의 모드설정(SetHeaderMode Method)
                Sort: 0, //type : number, 0:사용안함| 1:사용(default)|2:sort아이콘만표시|3:colspan 아닌 header셀만 sort
                ColMove: 0, //type : Boolean, 헤더컬럼 이동 가능여부
                ColResize: 1, //type : Boolean, 컬럼 너비 resize 여부
                HeaderCheck: 0 //type : Boolean, 헤더 전체체크 표시 여부
            },
            Cols: [ { //컬럼 정보 설정
                    Header: "조직코드",
                    Type: "Text", //컬럼 데이터타입(Text, Status, Int...)
                    Width: 50,
                    SaveName: "dept_cd", //데이터 저장 또는 조회시 사용하는 변수명 (Inorg에서 pkey)
                    Align: "Center", //정렬 : Left, Center, Right
                    Edit: 0, //type : Boolean, 편집가능여부
                    Hidden: 0 //type : Boolean, 컬럼 숨김여부
                },
                {
                    Header: "조직명",
                    Type: "Text",
                    Width: 100,
                    SaveName: "dept_nm",
                    Align: "Left",
                    Edit: 0,
                    TreeCol: 1
                },
                {
                    Header: "성명",
                    Type: "Text",
                    Width: 50,
                    SaveName: "emp_nm",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 0
                }
            ]
        }

        IBS_InitSheet( treeSheet, option ); // treeSheet는 IBSheet의 createIBSheet2에서 생성된 전역객체

        this.bind();


    },
    bind: function () {
        var that;
        that = this;

        function addEvents( treeSheet, callbacks ) {
            for ( var prop in callbacks ) {
                window[ treeSheet + "_" + prop ] = callbacks[ prop ];
            }
        }

        addEvents( treeSheet.id, {
            //IBSheet 이벤트는 외부에서 써야하나 내부에서 쓰기위해 addEvents 사용
            //현재 위치에 onClick : ~~ 등과 같이 사용
            OnSelectCell: function ( OldRow, OldCol, NewRow, NewCol, isDelete ) {
                //OnSelectCell Event : 특정 셀이 선택되었을 때 이벤트 발생
                //기존 선택되었던 셀, 새로선택된 셀 모두 반환하므로 선택된 셀에 대한 처리 가능
                //OldRow, OldCol : type(number) 이전 선택 셀의 row, column index
                //NewRow, NewCol : type(number) 현재 선택 셀의 row, column index
                //isDelete : type(Boolean) 추가된 행을 DelCheck로 삭제할경우, 같은 위치 기준으로 OnSelectCall 이벤트 발생하는데 이때 행삭제 됨을 구분해주기 위한 값
            },
            OnClick: function ( row ) {
                var select_key;
                select_key = treeSheet.GetCellValue( row, 0 );
                //org_view.click_sync( select_key );

                inapp.raise( that.name, {
                    action: "click_tree",
                    data: select_key
                } );


            },
            OnBeforeExpand: function ( row, expand ) {
                var select_key, expand_val;
                select_key = treeSheet.GetCellValue( row, 0 );

                if ( !expand ) {
                    expand_val = true;
                } else {
                    expand_val = false;
                }
                inapp.raise( that.name, {
                    action: "expand_tree",
                    data: {
                        key: select_key,
                        expand: expand_val
                    }
                } );
            }
        } );
    },
    click_sync: function ( select_key ) {
        var row;
        row = treeSheet.FindText( 0, select_key );
        if ( row < 0 ) { //방어코드
            return;
        }
        treeSheet.SetSelectRow( row );

    },
    expand_sync: function ( select_key, expand ) {
        var row;
        row = treeSheet.FindText( 0, select_key );
        if ( row < 0 ) {
            return;
        }
        treeSheet.SetRowExpanded( row, expand );

    },
    load: function ( jsonData ) {
        treeSheet.LoadSearchData( jsonData );
    }
} )