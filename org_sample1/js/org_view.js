    function initINOrg() {
        var option;

        option = {
            layout: {
                level: true, // 레벨 정렬 사용여부
                supporter: true // 보조자 사용여부
            },
            defaultTemplate: {
                link: { // 연결선 스타일
                    style: {
                        borderWidth: 2,
                        borderColor: "#CCC",
                        borderStyle: "solid"
                    }
                }
            },
            event: {
                onClick: function ( evt ) {

                    var node, jsonData;
                    jsonData = {};

                    // 수정전
                    // var node;
                    // jsonData = {}

                    if ( !evt.key ) { //키값있을때 노드만
                        return;
                    }
                    if ( evt.binding !== "emp_nm" ) { //노드의 이름값 선택시에만
                        return;
                    }
                    if ( evt.isMember == true ) {
                        node = evt.member;
                    } else {
                        node = evt.node;
                    }

                    node.fields().foreach( function () {
                        console.log( this.name(), this.value() );
                        jsonData[ this.name() ] = this.value();
                    } );

                    $( '#modalExample' ).modal( 'show' );
                    $( '#photo' ).attr( "src", jsonData.photo );
                    $( '#emp_nm' ).val( jsonData.emp_nm );
                    $( '#position' ).val( jsonData.position );
                    $( '#dept_nm' ).val( jsonData.dept_nm );

                    //수정전
                    // if ( evt.key ) { // = if(!evt.isOrg) //
                    //     if ( evt.isMember == true && evt.binding == "emp_nm" ) {
                    //         console.log( 'member' );
                    //         $( '#modalExample' ).modal( 'show' );
                    //         $( '#photo' ).attr( "src", evt.member.fields( "photo" ).value() );
                    //         $( '#emp_nm' ).val( evt.member.fields( "emp_nm" ).value() );
                    //         $( '#position' ).val( evt.member.fields( "position" ).value() );
                    //         $( '#dept_nm' ).val( evt.member.fields( "dept_nm" ).value() );
                    //         //label
                    //     } else if ( evt.node.isLabel() == true ) {
                    //         console.log('label' );
                    //         //node
                    //     } else if ( evt.binding == "emp_nm" ) {
                    //         $( '#modalExample' ).modal( 'show' );
                    //         $( '#photo' ).attr( "src", evt.node.fields( "photo" ).value() );
                    //         $( '#emp_nm' ).val( evt.node.fields( "emp_nm" ).value() );
                    //         $( '#position' ).val( evt.node.fields( "position" ).value() );
                    //         $( '#dept_nm' ).val( evt.node.fields( "dept_nm" ).value() );
                    //         // console.log( evt.node );
                    //         // console.log( evt.org );
                    //         // console.log( evt.node.fields( "dept_nm" ).value() );

                    //     }

                    // }
                }
            }
        }

        createINOrg( "viewOrg", option );

        load();
    }

    function load() {
        viewOrg.loadJson( {
            url: "./data/sample.json"
        } );
    }

    $( document ).ready( function () {
        console.log( "시작" );
        initINOrg();
    } );