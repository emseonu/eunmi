    var popup = {
        init: function () {
            // JQuery 버튼 클릭 메서드는 한번만 호출하면 된다.
            $( "#btnSave" ).on( "click", function () {
                // save 함수 호출
                popup.save();
            } );
        },
        /**
         * 팝업을 보여준다.
         */
        show: function ( getData ) {
            $( '#modalExample' ).modal( 'show' );
            $( '#pkey' ).val( getData.pkey );
            $( '#photo' ).attr( "src", getData.photo );
            $( '#emp_nm' ).val( getData.emp_nm );
            $( '#position' ).val( getData.position );
            $( '#dept_nm' ).val( getData.dept_nm );
            $( '#supporter' ).val( getData.supporter );
        },
        /**
         * 팝업 내에 저장 버튼을 누를때
         */
        save: function () {
            // 3단계 : 저장시 팝업 필드 정보를 선택된 노드에 반영
            var setData;

            setData = {
                "pkey": $( '#pkey' ).val(),
                "emp_nm": $( '#emp_nm' ).val(),
                "position": $( '#position' ).val(),
                "dept_nm": $( '#dept_nm' ).val(),
                "supporter": $( '#supporter' ).val()
            };

            org_view.save( setData );

        }
    };

    $( document ).ready( function () {
        popup.init();
    } );