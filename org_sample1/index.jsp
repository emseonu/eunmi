<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ko">

<head>
    <title>INORG Sample</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="./lib/bootstrap/css/bootstrap.min.css">

    <style>
        html,
        body {
            height: 100% !important;
        }
    </style>

    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="./lib/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- INORG 제품 -->
    <script type="text/javascript" src="./softin/softin.js"></script> <!-- 라이센스 정보 -->
    <script type="text/javascript" src="./softin/inorg/inorginfo.js"></script> <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="./softin/inorg/inorg.js"></script> <!-- core file -->

    <!-- 화면 소스 -->
    <script type="text/javascript" src="./js/org_view.js"></script>
</head>

<body>
    <!--조직도-->
    <div id="viewOrg" class="container-fluid" style="height: 100%;"></div>

    <!-- modal -->
    <div class="modal" id="modalExample" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">사원정보
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td rowspan="3"><input type="image" id="photo" src="" style="width:200px" /><br /></td>
                                <td>성명</td>
                                <td> <input type="text" class="form-control" id="emp_nm" value="" readonly /></td>
                            </tr>
                            <tr>
                                <td>직급</td>
                                <td><input type="text" class="form-control" id="position" value="" readonly /><br />
                                </td>

                            </tr>
                            <tr>
                                <td>부서명</td>
                                <td><input type="text" class="form-control" id="dept_nm" value="" readonly /></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>