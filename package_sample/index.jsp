<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ko">

<head>
    <title>INORG Sample</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="./lib/bootstrap/css/bootstrap.min.css">

    <style>
        html,
        body {
            height: 100% !important;
        }
    </style>

    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="./lib/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./lib/inapp.js"></script>

    <!-- INORG 제품 -->
    <script type="text/javascript" src="./softin/softin.js"></script> <!-- 라이센스 정보 -->
    <script type="text/javascript" src="./softin/inorg/inorginfo.js"></script> <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="./softin/inorg/inorg.js"></script> <!-- core file -->

    <!-- IBSheet 제품 -->
    <script type="text/javascript" src="./softin/ibsheet/ibsheet.js"></script>
    <script type="text/javascript" src="./softin/ibsheet/ibsheetinfo.js"></script>

    <!-- 화면 소스 -->
    <script type="text/javascript" src="./js/org_view.js"></script>
    <script type="text/javascript" src="./js/tree_view.js"></script>
    <script type="text/javascript" src="./js/app.js"></script>
    <script type="text/javascript" src="./js/popup.js"></script>

</head>

<body>
    <!-- 상단 버튼 -->
    <div id="buttonList" class="container-fluid form-inline">
        <select class="form-control" id="modi_temp" style="width: 200px;">
            <option value="">템플릿선택</option>
            <option value="OrgBox">OrgBox</option>
            <option value="PhotoBox">PhotoBox</option>
            <option value="FullBox">FullBox</option>
            <option value="HalfBox">HalfBox</option>
            <option value="VertiHalfBox">VertiHalfBox</option>
            <option value="listTemp">listTemp</option>
        </select>

        <button type="button" class="btn btn-secondary" id="zoomIn">확대</button>
        <button type="button" class="btn btn-secondary" id="zoomOut">축소</button>
        <button type="button" class="btn btn-secondary" id="scaleFit">기본크기</button>
        <button type="button" class="btn btn-secondary" id="scaleFullfit">화면맞추기</button>
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-secondary" id="modi_level_up">↑ </button>
            <button type="button" class="btn btn-secondary" disabled>레벨조절</button>
            <button type="button" class="btn btn-secondary" id="modi_level_down">↓</button>
        </div>
        <button type="button" class="btn btn-secondary" id="saveAsImage">이미지저장</button>
        <button type="button" class="btn btn-secondary" id="saveAsJson">JSON저장</button>
        <div class="btn-group" role="group">
            <button type="label" class="btn btn-secondary" disabled>레벨정렬</button>
            <select class="form-control" id="level_switch" style="width: 100px;">
                <option value="true">on</option>
                <option value="false">off</option>
            </select>
        </div>
        <div class="btn-group" role="group">
            <button type="label" class="btn btn-secondary" disabled>보조자정렬</button>
            <select class="form-control" id="support_switch" style="width: 100px;">
                <option value="true">on</option>
                <option value="false">off</option>
            </select>
        </div>
    </div>

    <div class="row" style="height:100%; margin: 0 !important;">
        <div class="col-md-3" style="height:100%;">
            <div id="treeArea"></div>
        </div>
        <div class="col-md-9" style="height:100%;">
            <div id="viewOrg" style="height: 100%;"></div>
        </div>
    </div>

    <!-- modal -->
    <div class="modal" id="modalExample" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">사원정보
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <input type="hidden" class="form-control" id="pkey" value="" />
                                <td rowspan="4"><input type="image" id="photo" src="" style="width:200px" /><br /></td>
                                <td>성명</td>
                                <td> <input type="text" class="form-control" id="emp_nm" value="" /></td>
                            </tr>
                            <tr>
                                <td>직급</td>
                                <td><input type="text" class="form-control" id="position" value="" /><br />
                                </td>

                            </tr>
                            <tr>
                                <td>부서명</td>
                                <td><input type="text" class="form-control" id="dept_nm" value="" /></td>
                            </tr>
                            <tr>
                                <td>보조자</td>
                                <td><input type="number" class="form-control" id="supporter" value="" /></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnSave" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>


</html>