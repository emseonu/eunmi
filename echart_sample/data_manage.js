inapp.add( "data_manage", {
    init: function () {
        this.manageing();
    },
    manageing: function () {
        var that, a, len, value, hours, minutes, time_func, yearResult, yearAvg, i, yeardata, salaryResult, salaryAvg, salarydata, incenResult, incenAvg, incendata, costs, costdata, p, personResult, persondata, rateA, rateB, rateC, rateD, rateE, ratedata, sexFe, sexM, sexdata, ageFe, ageM, ageFeAvg, ageMAvg, agedata, scFe, scM, scdata, typeA, typeB, typeC, typeD, typeE, typedata, classA, classB, classC, classD, classE, classdata, kindsA, kindsB, kindsC, kindsD, kindsE, kinddata, workArr, workNew, workAvg, workdata, vacaArr, vacadata, signArr, getArr, getNew, offNew, getValue, offValue, offArr, getdata, resignArr, signdata;

        that = this;

        //근속 : yearResult, yearAvg, i, yeardata, 연봉 :  salaryResult, salaryAvg, salarydata, 인센티브 : incenResult, incenAvg, incendata, 인건비 :  costs, costdata,인원 :p, personResult, persondata, 평가분포 : rateA, rateB, rateC, rateD, rateE, ratedata, 성비율 : sexFe, sexM, sexdata, 평균연령 : ageFe, ageM, ageFeAvg, ageMAvg, agedata, 성비율및인건비 : scFe, scM, scdata,직원유형 : typeA, typeB, typeC, typeD, typeE, typedata, 직군별 : classA, classB, classC, classD, classE, classdata, 직종별 : kindsA, kindsB, kindsC, kindsD, kindsE, kinddata, 평균근로 : workArr,workNew, workAvg,workdata,평균휴가 :  vacaArr, vacadata,출퇴근 :  getArr, offArr, getdata, getNew, offNew, 입퇴사 : resignArr, signdata

        $.get( 'chData.json' ).done( function ( data ) {
            yearResult = 0, i = 0, salaryResult = 0, incenResult = 0, costs = 0, p = 0, rateA = 0,
                rateB = 0, rateC = 0, rateD = 0, rateE = 0, sexFe = 0, sexM = 0, ageFe = 0, ageM = 0, scFe = 0, scM = 0, typeA = 0, typeB = 0, typeC = 0, typeD = 0, typeE = 0, classA = 0, classB = 0, classC = 0, classD = 0, classE = 0, kindsA = 0, kindsB = 0, kindsC = 0, kindsD = 0, kindsE = 0, workArr = [], workAvg = [], workNew = [], vacaArr = [], signArr = [], resignArr = [], getArr = [], offArr = [], getNew = [], offNew = [], getValue = [], offValue = [];


            $.each( data.person, function ( key, value ) {
                //근속, 연봉, 인센티브, 인건비 data
                i++;
                yearResult += value.year;
                salaryResult += value.salary;
                incenResult += value.incentive;
                costs += value.costs;

                //인원정보 (팀장제외) data
                if ( value.key != 0000 ) {
                    p++;
                }
                //평가분포 별 인원 data
                switch ( value.rating ) {
                    case 'A':
                        rateA++;
                        break;
                    case 'B':
                        rateB++;
                        break;
                    case 'C':
                        rateC++;
                        break;
                    case 'D':
                        rateD++;
                        break;
                    case 'E':
                        rateE++;
                        break;
                }

                //성비율 별 인원, 연령, 인건비 data
                switch ( value.sex ) {
                    case 'female':
                        sexFe++;
                        ageFe += value.age;
                        scFe += value.costs;
                        break;
                    case 'male':
                        sexM++;
                        ageM += value.age;
                        scM += value.costs;
                        break;
                }

                //직원유형 별 인원 data
                switch ( value.type ) {
                    case 'A':
                        typeA++;
                        break;
                    case 'B':
                        typeB++;
                        break;
                    case 'C':
                        typeC++;
                        break;
                    case 'D':
                        typeD++;
                        break;
                    case 'E':
                        typeE++;
                        break;
                }

                //직군별 인원 data
                switch ( value.class ) {
                    case 'A':
                        classA++;
                        break;
                    case 'B':
                        classB++;
                        break;
                    case 'C':
                        classC++;
                        break;
                    case 'D':
                        classD++;
                        break;
                    case 'E':
                        classE++;
                        break;
                }

                //직종별 인원data
                switch ( value.kinds ) {
                    case 'A':
                        kindsA++;
                        break;
                    case 'B':
                        kindsB++;
                        break;
                    case 'C':
                        kindsC++;
                        break;
                    case 'D':
                        kindsD++;
                        break;
                    case 'E':
                        kindsE++;
                        break;
                }
            } )

            //월별 평균 근로시간 data
            workAvg = [ 8, 8, 8.5, 8, 8.3, 8, 9.1, 8, 8, 8, 8, 8 ];
            $.each( data.work, function ( key, value ) {
                workArr.push( value );
            } );
            len = workArr.length;
            for ( a = 0; a < len; a++ ) {
                workNew[ a ] = ( workArr[ a ] - workAvg[ a ] ).toFixed( 0 );
            }

            //월별평균 휴가 사용일수 data
            $.each( data.vacation, function ( key, value ) {
                vacaArr.push( value );
            } );

            //출근 data
            $.each( data.get, function ( key, value ) {
                getArr.push( value );
            } );
            len = getArr.length;
            for ( a = 0; a < len; a++ ) {
                getNew.push( new Date( getArr[ a ] ) );
                getValue[ a ] = getNew[ a ].getTime();
            }

            //퇴근 data
            $.each( data.off, function ( key, value ) {
                offArr.push( value );
            } );
            len = offArr.length;
            for ( a = 0; a < len; a++ ) {
                offNew.push( new Date( offArr[ a ] ) );
                offValue[ a ] = offNew[ a ] - getNew[ a ];
            }

            //입사자 data
            $.each( data.sign, function ( key, value ) {
                signArr.push( value );
            } );

            //퇴사자 data
            $.each( data.resign, function ( key, value ) {
                resignArr.push( value );
            } );

            yearAvg = yearResult / i;
            salaryAvg = salaryResult / i;
            incenAvg = incenResult / i;

            personResult = p;

            ageFeAvg = ( ageFe / sexFe ).toFixed( 1 );
            ageMAvg = ( ageM / sexM ).toFixed( 1 );

            //근속년수
            yeardata = {
                "series": [ {
                    "data": [ yearAvg ],
                    "markArea": {
                        "silent": true,
                        "data": [
                            [ {
                                "xAxis": 71
                            }, {
                                "xAxis": 72
                            } ]
                        ],
                        "itemStyle": {
                            "color": "#cc0000"
                        }
                    }
                } ]
            };

            //연봉인상률
            salarydata = {
                series: [ {
                    data: [ salaryAvg ],
                    markArea: {
                        silent: true,
                        data: [
                            [ {
                                xAxis: '71'
                            }, {
                                xAxis: '72'
                            } ]
                        ],
                        itemStyle: {
                            color: '#cc0000'
                        }
                    }
                } ]
            };

            //인센티브지급률
            incendata = {
                series: [ {
                    data: [ incenAvg ],
                    markArea: {
                        silent: true,
                        data: [
                            [ {
                                xAxis: '71'
                            }, {
                                xAxis: '72'
                            } ]
                        ],
                        itemStyle: {
                            color: '#cc0000'
                        }
                    }
                } ]
            };

            //인건비 예산
            costdata = {
                title: {
                    subtext: '총예산\n 10,000만원',
                },
                legend: {
                    data: [ '집행예산', '가용예산' ]
                },
                series: [ {
                    name: 'costs',
                    data: [ {
                        name: '집행예산',
                        value: costs,
                        itemStyle: {
                            color: '#2f4554'
                        }
                    }, {
                        name: '가용예산',
                        value: 100000000 - costs,
                        itemStyle: {
                            color: '#efefef'
                        }
                    } ]
                } ]
            }

            //인원정보
            persondata = {
                legend: {
                    data: [ '팀장', '팀원' ]
                },
                xAxis: {
                    data: [ '팀장', '팀원' ],
                },
                series: [ {
                    data: [ {
                        name: '팀장',
                        value: 1,
                        symbol: 'image://./lib/person.png',
                        symbolSize: [ '150', '120' ],
                        label: {
                            position: 'right',
                            offset: [ -30, 0 ]
                        }
                    }, {
                        name: '팀원',
                        value: personResult,
                        symbol: 'image://./lib/person.png',
                        symbolSize: [ '30', '20' ],
                        symbolRepeat: true,
                        barCategoryGap: '30%',
                        label: {
                            position: 'left',
                            offset: [ 30, 0 ]
                        }

                    } ]
                } ]

            }

            //평가분포별 비율 및 인원
            ratedata = {
                legend: {
                    formatter: '{name}항목',
                    data: [ 'A', 'B', 'C', 'D', 'E' ]
                },
                series: [ {
                    data: [ {
                            name: 'A',
                            value: rateA
                        },
                        {
                            name: 'B',
                            value: rateB
                        },
                        {
                            name: 'C',
                            value: rateC
                        },
                        {
                            name: 'D',
                            value: rateD
                        },
                        {
                            name: 'E',
                            value: rateE
                        }
                    ]
                } ]
            }

            //성비율 및 인원
            sexdata = {
                legend: {
                    data: [ '여성', '남성' ]
                },
                series: [ {
                    data: [ {
                            name: '여성',
                            value: sexFe
                        },
                        {
                            name: '남성',
                            value: sexM
                        }
                    ]
                } ]
            }

            //평균연령
            agedata = {
                xAxis: {
                    data: [ '여성', '남성' ]
                },
                series: [ {
                    data: [ {
                        name: '남성',
                        value: ageMAvg,
                        symbol: 'image://./lib/man.png',
                        symbolSize: [ '150', '120' ]
                    }, {
                        name: '여성',
                        value: ageFeAvg,
                        symbol: 'image://./lib/woman.png',
                        symbolSize: [ '150', '120' ]


                    } ]
                } ]
            }

            //성비율 및 인건비
            scdata = {
                legend: {
                    data: [ '여성', '남성' ]
                },
                series: [ {
                    data: [ {
                            name: '여성',
                            value: scFe
                        },
                        {
                            name: '남성',
                            value: scM
                        }
                    ]
                } ]
            }

            //직원유형별 비율 및 인원
            typedata = {
                legend: {
                    formatter: '{name}항목',
                    data: [ 'A', 'B', 'C', 'D', 'E' ]
                },
                series: [ {
                    data: [ {
                            name: 'A',
                            value: typeA
                        },
                        {
                            name: 'B',
                            value: typeB
                        },
                        {
                            name: 'C',
                            value: typeC
                        },
                        {
                            name: 'D',
                            value: typeD
                        },
                        {
                            name: 'E',
                            value: typeE
                        }
                    ]
                } ]
            }

            //직군별 비율 및 인원
            classdata = {
                legend: {
                    formatter: '{name}항목',
                    data: [ 'A', 'B', 'C', 'D', 'E' ]
                },
                series: [ {
                    data: [ {
                            name: 'A',
                            value: classA
                        },
                        {
                            name: 'B',
                            value: classB
                        },
                        {
                            name: 'C',
                            value: classC
                        },
                        {
                            name: 'D',
                            value: classD
                        },
                        {
                            name: 'E',
                            value: classE
                        }
                    ]
                } ]
            }

            //직종별 비율 및 인원
            kinddata = {
                legend: {
                    formatter: '{name}항목',
                    data: [ 'A', 'B', 'C', 'D', 'E' ]
                },
                series: [ {
                    data: [ {
                            name: 'A',
                            value: kindsA
                        },
                        {
                            name: 'B',
                            value: kindsB
                        },
                        {
                            name: 'C',
                            value: kindsC
                        },
                        {
                            name: 'D',
                            value: kindsD
                        },
                        {
                            name: 'E',
                            value: kindsE
                        }
                    ]
                } ]
            }

            //월별 평균 근로시간
            workdata = {
                legend: {
                    data: [ '소정', '시간외' ],
                },
                yAxis: {
                    name: '평균근로시간(H)'

                },
                series: [ {
                        name: '소정',
                        type: 'bar',
                        stack: '1',
                        label: {
                            color: '#333',
                            textBorderColor: '#fff',
                        },
                        itemStyle: {
                            color: '#61a0a8'
                        },
                        data: workAvg
                    },
                    {
                        name: '시간외',
                        type: 'bar',
                        stack: '1',
                        label: {
                            color: '#fff',
                            textBorderColor: '#333',

                        },
                        itemStyle: {
                            color: '#2f4554'
                        },
                        data: workNew
                    }
                ]
            }


            //인당월별 평균 휴가 사용일수
            vacadata = {
                legend: {
                    data: [ '휴가사용일' ],
                },
                yAxis: {
                    name: '평균휴가사용일(일)'
                },
                series: [ {
                    data: vacaArr,
                    itemStyle: {
                        color: '#61a0a8'
                    },
                    label: {
                        show: true,
                        color: '#333',
                        textBorderColor: '#fff',
                        textBorderWidth: 2,
                        position: 'top'
                    }
                } ]
            }


            time_func = function ( params ) {
                value = new Date( params[ 'data' ] );
                hours = value.getHours();
                minutes = value.getMinutes();
                if ( value.getMinutes().toString().length < 2 ) {
                    minutes = "0" + value.getMinutes();
                } else {
                    minutes = value.getMinutes();
                }
                return hours + ":" + minutes;
            }

            //월별 평균 출퇴근시간
            getdata = {
                tooltip: {
                    formatter: time_func

                },
                label: {
                    formatter: time_func,
                    show: true,
                    color: '#333',
                    textBorderColor: '#fff',
                    textBorderWidth: 2
                },
                legend: {
                    data: [ '', '출퇴근시간' ]
                },
                yAxis: [ {
                    name: '출퇴근시간(H)',
                    nameLocation: 'middle',
                    nameGap: 40,
                    splitNumber: 8,
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    min: new Date( 'Sun May 03 2020 00:00:00' ).getTime(),
                    max: new Date( 'Sun May 03 2020 24:00:00' ).getTime(),
                    type: 'value',
                    axisLabel: {
                        formatter: function ( params ) {
                            value = new Date( params );
                            hours = value.getHours();
                            minutes = value.getMinutes();

                            if ( value.getMinutes().toString().length < 2 ) {
                                minutes = "0" + value.getMinutes();
                            } else {
                                minutes = value.getMinutes();
                            }
                            return hours + ":" + minutes;
                        }

                    }
                } ],
                series: [ {
                    name: '',
                    type: 'bar',
                    stack: '2',
                    itemStyle: {
                        barBorderColor: '#fff',
                        color: '#fff'
                    },
                    emphasis: {
                        itemStyle: {
                            barBorderColor: '#fff',
                            color: '#fff'
                        }
                    },
                    label: {
                        position: 'insideTop'
                    },
                    data: getValue
                }, {
                    name: '출퇴근시간',
                    type: 'bar',
                    stack: '2',
                    label: {
                        position: 'top'
                    },
                    itemStyle: {
                        color: '#61a0a8'
                    },
                    data: offValue
                } ]
            }

            //월별 입/퇴사자수
            signdata = {
                barWidth: '25%',
                legend: {
                    data: [ '입사자', '퇴사자' ],
                },
                label: {
                    show: true,
                    color: '#333',
                    textBorderColor: '#fff',
                    textBorderWidth: 2,
                    position: 'top'
                },
                yAxis: {
                    name: '인원(명)'
                },
                series: [ {
                        name: '입사자',
                        type: 'bar',
                        itemStyle: {
                            color: '#61a0a8'
                        },
                        data: signArr
                    },
                    {
                        name: '퇴사자',
                        type: 'bar',
                        itemStyle: {
                            color: '#2f4554'
                        },
                        data: resignArr

                    }
                ]

            }

            inapp.raise( that.name, {
                action: "data_loaded",
                data: {
                    "year": yeardata,
                    "salary": salarydata,
                    "incen": incendata,
                    "cost": costdata,
                    "person": persondata,
                    "rate": ratedata,
                    "sex": sexdata,
                    "age": agedata,
                    "sc": scdata,
                    "type": typedata,
                    "class": classdata,
                    "kind": kinddata,
                    "work": workdata,
                    "vaca": vacadata,
                    "get": getdata,
                    "sign": signdata
                }
            } )
        } )
    }
} )