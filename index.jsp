<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.io.File,java.io.IOException,java.util.*,java.io.*" %>

<!DOCTYPE>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Directory List</title>
  <link href="org_sample1/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
  <div class="container-fluid">
    <h3 class="page-header">선우은미 - 제품교육 리스트</h3>
    <%
      String fileDir = "";
      String filePath = application.getRealPath("/");
      filePath = filePath.replace( "\\" , "/" ) + "/" + fileDir;

      File f = new File(filePath);

      File[] files = f.listFiles();

      for (File file : files) {
          if (file.isDirectory() && (file.getName()).indexOf(".") != 0) {
    %>
    <div style="margin-right: 8px;">
      <a href="${pageContext.request.contextPath}<%=fileDir%>/<%=file.getName() %>"
        target="_blank" class="btn btn-default btn-lg">
        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        <%=file.getName() %>
      </a>
    </div>
    <%
      }
    }
    %>
  </div>
</body>

</html>