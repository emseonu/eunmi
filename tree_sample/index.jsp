<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ko">

<head>
    <title>INORG Sample</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="./lib/bootstrap/css/bootstrap.min.css">

    <style>
        html,
        body {
            height: 100% !important;
        }
    </style>

    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="./lib/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./lib/inapp.js"></script>

    <!-- INORG 제품 -->
    <script type="text/javascript" src="./softin/softin.js"></script> <!-- 라이센스 정보 -->
    <script type="text/javascript" src="./softin/inorg/inorginfo.js"></script> <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="./softin/inorg/inorg.js"></script> <!-- core file -->

    <!-- IBSheet 제품 -->
    <script type="text/javascript" src="./softin/ibsheet/ibsheet.js"></script>
    <script type="text/javascript" src="./softin/ibsheet/ibsheetinfo.js"></script>

    <!-- 화면 소스 -->
    <script type="text/javascript" src="./js/org_view.js"></script>
    <script type="text/javascript" src="./js/tree_view.js"></script>
    <script type="text/javascript" src="./js/app.js"></script>

</head>

<body>
    <div class="row" style="height:100%; margin: 0 !important;">
        <div class="col-md-3" style="height:100%;">
            <div id="treeArea"></div>
        </div>
        <div class="col-md-9" style="height:100%;">
            <div id="viewOrg" style="height: 100%;"></div>
        </div>
    </div>
</body>


</html>